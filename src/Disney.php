<?php
/**
  * Class for accessing DOM data representation of the contents of a Disney.xml
  * file
  */
class Disney
{
    /**
      * The object model holding the content of the XML file.
      * @var DOMDocument
      */
    protected $doc;

    /**
      * An XPath object that simplifies the use of XPath for finding nodes.
      * @var DOMXPath
      */
    protected $xpath;

    /**
      * param String $url The URL of the Disney XML file
      */
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
        $this->xpath = new DOMXPath($this->doc);
    }

    /**
      * Creates an array structure listing all actors and the roles they have
      * played in various movies.
      * returns Array The function returns an array of arrays. The keys of they
      *               "outer" associative array are the names of the actors.
      *                The values are numeric arrays where each array lists
      *                key information about the roles that the actor has
      *                played. The elments of the "inner" arrays are string
      *                formatted this way:
      *               'As <role name> in <movie name> (movie year)' - such as:
      *               array(
      *               "Robert Downey Jr." => array(
      *                  "As Tony Stark in Iron Man (2008)",
      *                  "As Tony Stark in Spider-Man: Homecoming (2017)",
      *                  "As Tony Stark in Avengers: Infinity War (2018)",
      *                  "As Tony Stark in Avengers: Endgame (2019)"),
      *               "Terrence Howard" => array(
      *                  "As Rhodey in Iron Man (2008)")
      *               )
      */
    public function getActorStatistics()
    {
		$result = array();
		
		//$movies = array();
		$actors = $this->xpath->query("Actors/Actor/Name");
		$actorId = $this->xpath->query("Actors/Actor/@id");
		$movies = $this->xpath->query("Subsidiaries/Subsidiary/Movie/Name");
		$years = $this->xpath->query("Subsidiaries/Subsidiary/Movie/Year");
		for($i = 0; $i<$actors->length; $i++){
			$roles = array();
			for($j = 0; $j<$movies->length; $j++){
				$movie = $movies[$j]->nodeValue;
				$year = $years[$j]->nodeValue;
				$actor = $this->xpath->query("Subsidiaries/Subsidiary/Movie[Name = '$movie' and Year = '$year']/Cast/Role/@actor");
				$role = $this->xpath->query("Subsidiaries/Subsidiary/Movie[Name = '$movie']/Cast/Role/@name");
			
				for($k = 0; $k<$actor->length; $k++){
					$role2 = $role[$k]->nodeValue;
						if($actor[$k]->nodeValue == $actorId[$i]->nodeValue){
							$roles[] = "As $role2 in $movie ($year)";}
			}
			
		}
		$result[$actors[$i]->nodeValue] = ($roles);
		unset($roles);}

        return $result;
    }

    /**
      * Removes Actor elements from the $doc object for Actors that have not
      * played in any of the movies in $doc - i.e., their id's do not appear
      * in any of the Movie/Cast/Role/@actor attributes in $doc.
      */
    public function removeUnreferencedActors()
    {
		
		$actors = $this->xpath->query("Actors/Actor");
		$actorId = $this->xpath->query("Actors/Actor/@id");
		for($i = 0; $i<$actors->length; $i++){
			$bool = FALSE;
			foreach($this->xpath->query("Subsidiaries/Subsidiary/Movie/Cast/Role/@actor") as $role)
			{
				if($actorId[$i]->nodeValue == $role->nodeValue)
					$bool = TRUE;
			}
			if($bool==FALSE)
			{
				$actors[$i]->parentNode->removeChild($actors[$i]);
			}
		}
        //To do:
        // Implement functionality as specified
		
    }

    /**
      * Adds a new role to a movie in the $doc object.
      * @param String $subsidiaryId The id of the Disney subsidiary
      * @param String $movieName    The name of the movie of the new role
      * @param Integer $movieYear   The production year of the given movie
      * @param String $roleName     The name of the role to be added
      * @param String $roleActor    The id of the actor playing the role
      * @param String $roleAlias    The role's alias (optional)
      */
    public function addRole($subsidiaryId, $movieName, $movieYear, $roleName,
                            $roleActor, $roleAlias = null)
    {
		$movies = $this->xpath->query("Subsidiaries/Subsidiary[@id = '$subsidiaryId']/Movie[Name = '$movieName' and Year = '$movieYear']/Cast");
		//$years = $this->xpath->query("Subsidiaries/Subsidiary[@id = '$subsidiaryId']/Movie[Name = '$movieName']");
		//$name = $this->xpath->query(
		$role = $this->doc->createElement('Role');
		$role->setAttribute('name', $roleName);
		if($roleAlias != NULL)
			$role->setAttribute('alias', $roleAlias);
		$role->setAttribute('actor', $roleActor);
		$movies[0]->appendChild($role);
		
		//$this = $this->doc->appendChild($role);
        //To do:
        // Implement functionality as specified

    }
}
?>
